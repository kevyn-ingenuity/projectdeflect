//
// 2DKit - Rapid game development
// http://2dkit.com

package projectDeflect;

import kit.Component;
import kit.Entity;
import kit.System;

import ez.EzApp;

import projectDeflect.DeflectSystemsSetup;
import ecs.setup.ECSSetup;

/** The main component class. */
class Main extends Component
{
    /** Called when the component is started. */
    override public function onStart ()
    {
        var ez = new EzApp("Home");

        ez.loaded.connect(function()
        {
            var setup = new ECSSetup();
            setup.onSetupComplete.connect(function(world)
            {
                DeflectSystemsSetup.AddSystems(world);
            });
            owner.add(setup);
        });

        owner.add(ez);

    }
}
