package projectDeflect;

import edge.*;

import ecs.id.*;
import ecs.setup.World;

/**
 *  SETUP THE REQUIRED SYSTEMS FOR YOUR CURRENT PROJECT. THIS IS ENGINE INDEPENDENT
 */
class DeflectSystemsSetup
{
    public static function AddSystems(world : World)
    {
        IDSystems.Setup(world);
        //add features here
    }
}