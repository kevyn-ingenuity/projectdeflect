package ecs.id;

import edge.*;

import ecs.entityIndex.PrimaryEntityIndex;

//Helper function of CptID indexing and quick searching
class IDHelper
{
    static var _index : PrimaryEntityIndex<Entity, Int> = new PrimaryEntityIndex<Entity, Int>();

    public static function Get (id : Int) : Entity
    {
        return _index.Get(id);
    }

    public static function Add (id : Int, entity : Entity)
    {
        _index.Add(id, entity);
    }

    public static function Remove(id : Int)
    {
        _index.Remove(id);
    }

}