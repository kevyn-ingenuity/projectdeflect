package ecs.id;

import ecs.id.*;

import edge.*;

import ecs.setup.World;

class IDSystems
{
    public static function Setup (world : World)
    {
        world.frame.add(new IDIndexingAndFactorySystem());
        world.frame.add(new ExtensionIDIndexingSystem());
    }
}