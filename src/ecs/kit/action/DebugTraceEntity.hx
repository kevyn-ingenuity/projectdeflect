package ecs.kit.action;

import kit.creator.CreatorAction;
import kit.creator.CreatorObject;

import kit.Entity;
import kit.System;

import ecs.object.ECSEntity;

import edge.*;

/**
 *  TRACE THE ECS ENTITY STACK
 */
class DebugTraceEntity extends CreatorAction
{
    override public function onRun(target : Entity)
    {
        var entity : edge.Entity = target.get(ECSEntity).instance;
        trace(entity);
    }
}