package ecs.components;

import edge.*;

import kit.util.SignalConnection;
import kit.input.Key;

class KeyInputUp implements IComponent
{
    var event : String;
    var key : Key;
    var connection : SignalConnection;
}