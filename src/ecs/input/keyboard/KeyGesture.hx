package ecs.input.keyboard;

enum KeyGesture
{
    None;
    Down;
    Up;
}