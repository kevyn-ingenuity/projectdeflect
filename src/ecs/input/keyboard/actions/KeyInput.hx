package ecs.input.keyboard.actions;

import edge.*;

import kit.System;
import kit.creator.CreatorAction;

import kit.Entity;

import ecs.object.ECSEntity;
import ecs.components.*;

import ecs.input.keyboard.KeyGesture;
import kit.input.Key;

class KeyInput extends CreatorAction
{
    public var event : String = "";
    public var key : Key = Key.A;
    public var gesture : KeyGesture = KeyGesture.None;

    override public function onRun(target : Entity)
    {
        if (System.keyboard.supported == false){ return; }

        var entity : edge.Entity = target.get(ECSEntity).instance.engine.create();

        switch gesture
        {
            case Down:
                entity.add(new KeyInputDown(event, key, null));
            case Up:
                entity.add(new KeyInputUp(event, key, null));
            default: 
                trace ("no gesture to emit");
        }
        
    }
}

