package ecs.input.keyboard;

import ecs.input.keyboard.systems.*;

import edge.*;

import ecs.setup.World;

class InputKeyboardSystems
{
    public static function Setup (world : World)
    {
        world.input.add(new EmitKeyDownDataSystem());
        world.input.add(new EmitKeyUpDataSystem());

        world.cleanup.add(new CleanupKeyDataSystem());
    }
}