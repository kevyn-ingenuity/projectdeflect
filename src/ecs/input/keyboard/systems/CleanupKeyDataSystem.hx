package ecs.input.keyboard.systems;

import edge.*;

import ecs.components.*;

class CleanupKeyDataSystem implements ISystem
{
    var entity : Entity;

    public function update(data : KeyData)
    {
        trace(data);
        entity.remove(data);
    }
}