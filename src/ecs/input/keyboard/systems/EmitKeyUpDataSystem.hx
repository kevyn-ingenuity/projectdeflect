package ecs.input.keyboard.systems;

import ecs.components.*;

import kit.System;
import kit.input.KeyboardEvent;

using hxLINQ.LINQ;

import edge.*;

class EmitKeyUpDataSystem implements ISystem
{
    var listeners : edge.View<{ ltr : ListenToKeyInput }>;

    public function updateAdded(entity : Entity, data : { event : KeyInputUp })
    {
        data.event.connection = System.keyboard.up.connect(function (keyData){
            
            //filter entities
            var validLtrs : Array<edge.Entity> = listeners.linq()
                .selectMany(function(ltr, index)
                {
                    return ltr.data.ltr.eventList.linq()
                        .where(function(event, index)
                        {
                            return event == data.event.event && keyData.key == data.event.key;
                        })
                        .select(function(ptrData, index) return ltr.entity).toArray();
                }).toArray();

            //add data to entities that passed the filtering
            for(entity in validLtrs)
            {
                if(entity.existsType(KeyData))
                {
                    var currPtrData : Map<String, KeyboardEvent> = entity.get(KeyData).data;
                    if (currPtrData.exists(data.event.event))
                    {
                        currPtrData[data.event.event] = keyData;
                    }
                    else
                    {
                        currPtrData.set(data.event.event, keyData);
                    }
                }
                else
                {
                    var newData : Map<String, KeyboardEvent> = new Map();
                    newData.set(data.event.event, keyData);
                    entity.add(new KeyData(newData));
                }
            }

        });
    }

    public function update (event : KeyInputUp) {}

    public function updateRemoved(entity : Entity, data : { event : KeyInputUp })
    {
        data.event.connection.dispose();
    }

}