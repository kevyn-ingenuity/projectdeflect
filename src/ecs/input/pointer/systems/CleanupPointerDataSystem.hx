package ecs.input.pointer.systems;

import edge.*;

import ecs.components.*;

class CleanupPointerDataSystem implements ISystem
{
    var entity : Entity;

    public function update(data : PointerData)
    {
        trace(data);
        entity.remove(data);
    }
}