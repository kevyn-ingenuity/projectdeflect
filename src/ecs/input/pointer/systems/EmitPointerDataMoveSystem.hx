package ecs.input.pointer.systems;

import edge.*;
using hxLINQ.LINQ;

import kit.System;
import kit.display.Sprite;
import kit.input.PointerEvent;

import ecs.components.*;

class EmitPointerDataMoveSystem implements ISystem
{
    var listeners : edge.View<{ ltr : ListenToPointerInput }>;

    public function updateAdded(entity : Entity, data : { event : PointerInputMove })
    {
        data.event.connection = System.pointer.move.connect(function (pointerData){

            //filter entities
            var validLtrs : Array<edge.Entity> = listeners.linq()
                .selectMany(function(ltr, index)
                {
                    return ltr.data.ltr.eventList.linq()
                        .where(function(ptrData, index)
                        {
                            return ptrData.event == data.event.event;
                        })
                        .where(function(ptrData, index)
                        {
                            if (ptrData.global == false)
                            {
                                try
                                {
                                    var sprite = ltr.entity.existsType(KitSprite) ? ltr.entity.get(KitSprite).instance : throw(entity);
                                    if (sprite == pointerData.hit)
                                    {
                                        return true;
                                    }
                                    return false;
                                }
                                catch(msg : Entity)
                                {
                                    trace("listener listed as non-global, but no sprite found for check.\n" + msg);
                                }
                            }

                            return true;
                        })
                        .select(function(ptrData, index) return ltr.entity).toArray();
                }).toArray();

                //add data to entities that passed the filtering
                for(entity in validLtrs)
                {
                    if(entity.existsType(PointerData))
                    {
                        var currPtrData : Map<String, PointerEvent> = entity.get(PointerData).data;
                        if (currPtrData.exists(data.event.event))
                        {
                            currPtrData[data.event.event] = pointerData;
                        }
                        else
                        {
                            currPtrData.set(data.event.event, pointerData);
                        }
                    }
                    else
                    {
                        var newData : Map<String, PointerEvent> = new Map();
                        newData.set(data.event.event, pointerData);
                        entity.add(new PointerData(newData));
                    }
                }

        });
    }

    public function update (event : PointerInputMove) {}

    public function updateRemoved(entity : Entity, data : { event : PointerInputMove })
    {
        data.event.connection.dispose();
    }

}