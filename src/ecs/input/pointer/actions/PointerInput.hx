package ecs.input.pointer.actions;

import edge.*;

import kit.System;
import kit.creator.CreatorAction;

import kit.Entity;

import ecs.object.ECSEntity;
import ecs.components.*;

import ecs.input.pointer.PointerGesture;

class PointerInput extends CreatorAction
{
    public var event : String = "";
    public var gesture : PointerGesture = PointerGesture.None;

    override public function onRun(target : Entity)
    {
        if (System.pointer.supported == false){ return; }

        var entity : edge.Entity = target.get(ECSEntity).instance.engine.create();

        switch gesture
        {
            case Down:
                entity.add(new PointerInputDown(event, null));
            case Move:
                entity.add(new PointerInputMove(event, null));
                trace("added move");
            case Up:
                entity.add(new PointerInputUp(event, null));
            default: 
                trace ("no gesture to emit");
        }
        
    }
}

