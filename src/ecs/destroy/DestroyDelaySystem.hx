package ecs.destroy;

import edge.*;

import ecs.components.*;

class DestroyDelaySystem implements ISystem
{
    public function updateAdded(entity : Entity, data :{ destroy : Destroy })
    {
        data.destroy.signal.finally(function()
        {
            entity.destroy();
        });
    }
    public function update(destroy : Destroy) {}
}