package ecs.save_load;

import edge.*;

import kit.util.Promise;

import ecs.components.*;

class CleanupLoadProgressSystem implements ISystem
{
    var entity : Entity;
    
    public function updateAdded(entity : Entity, data : { progress : LoadProgress })
    {
        data.progress.progress.finally(function()
        {
            entity.removeType(LoadProgress);
        });
    }

    public function update (progress : LoadProgress){}

}