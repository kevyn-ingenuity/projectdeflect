package ecs.save_load;

import edge.*;

import kit.util.Promise;

import ecs.components.*;

class CleanupSaveProgressSystem implements ISystem
{
    var entity : Entity;

    public function updateAdded(entity : Entity, data : { progress : SaveProgress })
    {
        data.progress.progress.finally(function()
        {
            entity.removeType(SaveProgress);
        });
    }

    public function update (progress : SaveProgress) {}
}