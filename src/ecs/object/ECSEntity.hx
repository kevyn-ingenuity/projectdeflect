package ecs.object;

import kit.creator.CreatorObject;
import kit.System;
import kit.Component;
import kit.scene.Director;
import kit.creator.SceneInfo;

import edge.*;

import ecs.setup.ECSSetup;
import ecs.components.*;

/**
 *  THE TYPE THE OBJECTS SHOULD HAVE IN THE CREATOR SO THEY CAN BE READ BY THE ECS SYSTEM
 */
class ECSEntity extends Component
{
    public var instance(default, null) : edge.Entity;
    public var engine (default, null): edge.Engine;

    public function new (owner : kit.Entity)
    {
        var engine : edge.Engine = System.root.get(ECSSetup).world.engine;
        this.engine = engine;
        instance = engine.create(
            [new Id(-1)]);
        owner.add(this);
    }
}