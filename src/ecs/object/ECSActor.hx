package ecs.object;

import ez.Actor;

import ecs.object.ECSEntity;

/**
 *  THE TYPE THE OBJECTS SHOULD HAVE IN THE CREATOR SO THEY CAN BE READ BY THE ECS SYSTEM
 */
class ECSActor extends Actor
{
    public var initComponents : String;

    public var entity(default, null) : ECSEntity;

    override public function onStart()
    {
        entity = new ECSEntity(owner);

        if (initComponents != null)
        {
            owner.emitMessageToParents(initComponents, owner);
        }
    }
}